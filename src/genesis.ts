import Block from './block';

const genesisBlockHash: string = `
bcbfe4a2e843558c70b268d82399d1874e7035be4fb14ae05ecc4ec11fdbd2bf
`.replace(/\s/g,'')

const genesisBlock = new Block(
    0,
    null,
    1620500920173,
    "THE ORIGIN OF MONEY",
    genesisBlockHash
)

export default genesisBlock;