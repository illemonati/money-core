import Block from "./block";
import genesisBlock from "./genesis";

class BlockChain {
    blocks: Block[] = [];
    constructor(blocks?: Block[]) {
        this.blocks = blocks || [];
    }

    get length(): number {
        return this.blocks.length;
    }
    
    isGenesisValid() {
        return this.blocks && (JSON.stringify(this.blocks[0]) === JSON.stringify(genesisBlock));
    } 

    isValid() {
        if (!this.isGenesisValid()) return false;
        for (let i = 1; i < this.blocks.length; i++) {
            if (!this.blocks[i].isValid(this.blocks[i-1])) {
                return false;
            }
        }
        return true;
    }

    replaceChain(newChain: BlockChain) {
        if (newChain.isValid() && newChain.length > this.length) {
            this.blocks = newChain.blocks
        } else {
            throw new Error("Invalid Replacement")
        }
    }
}

export default BlockChain;