import sha256 from 'crypto-js/sha256';
import Base64 from 'crypto-js/enc-base64';
import { TextEncoder } from 'text-encoding';

class Block {
    public index: number;
    public hash: string;
    public previousHash: string | null;
    public timestamp: number;
    public data: Uint8Array;

    constructor(index: number, previousHash: string | null, timestamp: number, data: Uint8Array | string, hash?: string) {
        this.index = index;
        this.previousHash = previousHash;
        this.timestamp = timestamp;
        if (typeof data === 'string') {
            const textEncoder = new TextEncoder();
            data = textEncoder.encode(data);
        }
        this.data = data;
        this.hash = hash || this.calculateHash();
    }

    calculateHash(): string {
        return Base64.stringify(sha256(this.index + (this.previousHash || "") + this.timestamp + this.data));
    }

    generateNextBlock(data: Uint8Array | string): Block {
        const newIndex = this.index + 1;
        const previousHash = this.hash;
        const newTimestamp = Date.now();
        return new Block(newIndex, previousHash, newTimestamp, data);
    }

    isStructureValid(): boolean {
        return typeof this.index === "number"
        && typeof this.hash === "string"
        && ((this.previousHash === null) || typeof this.previousHash === "string")
        && typeof this.timestamp === "number"
        && this.data.constructor === Uint8Array
    }

    isValid(previousBlock: Block): boolean {
        return this.isStructureValid()
        && previousBlock.index+1 == this.index
        && previousBlock.hash == this.previousHash
        && this.hash == this.calculateHash()
    }
}

export default Block;